//! This module provides a couple Runge Kutta based functions to find
//! approximate solutions to ordinary differential equations.

/// Fourth order Runge Kutta solution to a first-order initial value problem of
/// the form:
///
/// `y' = f(t, y), a <= t <= b, y(a) = alpha`
///
/// at `(n + 1)` equally spaced numbers in the interval `[a, b]`.
///
/// The local truncation error is `O(h^4)`, where `h = (b - a) / n`.  This
/// means the error of the final result can be controlled by selecting an
/// appropriate number of steps, `n`, at the expense of additional computation
/// time for higher choices of `n`.
///
/// This solution is an implementation of Algorithm 5.2 from Burden's
/// "Numerical Analysis", 9th edition.
///
/// # Arguments
///
/// * `f` - Right hand side of the ODE to be solved.
/// * `n` - Number of iteration steps to take in solution.
/// * `alpha` - Initial value of `y` at `t = a`.
/// * `a` - Initial value of `t`, our independent variable.
/// * `b` - Final value of `t`, our independent variable.
///
/// # Examples
///
/// ```
/// use rk4::*;
///
/// let alpha = 0.5;
/// let a = 0.0;
/// let b = 2.0;
/// let n = 10;
///
/// // Burden, Example 3, pg. 289.
/// let result = rk4_1d(|t, w| w - t * t + 1.0, n, alpha, a, b);
/// ```
pub fn rk4_1d(f: impl Fn(f64, f64) -> f64, n: u32, alpha: f64, a: f64, b: f64) -> Vec<(f64, f64)> {
    // Step 1 in Burden
    let h = (b - a) / (n as f64);
    let mut t = a;
    let mut w = alpha;
    let mut output = vec![];
    output.push((t, w));

    // Step 2 in Burden
    for _ in 0..n {
        // Step 3 in Burden
        let k1 = h * f(t, w);
        let k2 = h * f(t + h / 2.0, w + k1 / 2.0);
        let k3 = h * f(t + h / 2.0, w + k2 / 2.0);
        let k4 = h * f(t + h, w + k3);

        // Step 4 in Burden
        w += (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0;
        t += h;

        // Step 5 in Burden
        output.push((t, w));
    }
    output
}

/// Fourth order Runge Kutta solution to an `m`th-order system of first-order
/// initial value problems of the form:
///
/// `u_j' = f_j(t, u_1, u_2, ..., u_m), a <= t <= b, u_j(a) = alpha_j`
///
/// for `j = 1, 2, ..., m` at `(n + 1)` equally spaced numbers in the interval
/// `[a, b]`.
///
/// The local truncation error is `O(h^4)`, where `h = (b - a) / n`.  This
/// means the error of the final result can be controlled by selecting an
/// appropriate number of steps, `n`, at the expense of additional computation
/// time for higher choices of `n`.
///
/// This solution is an implementation of Algorithm 5.7 from Burden's
/// "Numerical Analysis", 9th edition.
///
/// # Arguments
///
/// * `f` - First order functions defining the right hand side of the system of
///         ODEs to be solved.
/// * `n` - Number of iteration steps to take in solution.
/// * `alpha` - Initial value of `y` at `t = a`.
/// * `a` - Initial value of `t`, our independent variable.
/// * `b` - Final value of `t`, our independent variable.
///
/// # Examples
///
/// ```
/// use rk4::*;
///
/// // Burden, Illustration, pg. 331.
/// let mut f: Vec<fn(f64, Vec<f64>) -> f64> = vec![];
/// f.push(|_t, w| -4.0 * w[0] + 3.0 * w[1] + 6.0 );
/// f.push(|_t, w| -2.4 * w[0] + 1.6 * w[1] + 3.6 );
/// let n = 5;
/// let a = 0.0;
/// let b = 0.5;
/// let alpha = vec![0.0, 0.0];
///
/// let result = rk4_nd(f, n, alpha, a, b);
/// ```
pub fn rk4_nd(
    f: Vec<impl Fn(f64, Vec<f64>) -> f64>,
    n: u32,
    alpha: Vec<f64>,
    a: f64,
    b: f64,
) -> Vec<(f64, Vec<f64>)> {
    let m = f.len();

    // Step 1 in Burden
    let h = (b - a) / (n as f64);
    let mut t = a;

    // Step 2 in Burden
    let mut w = alpha.clone();

    // Step 3 in Burden
    let mut output = vec![];
    output.push((t, w.clone()));

    // Step 4 in Burden
    for _ in 0..n {
        // Step 5 in Burden
        let mut k1 = vec![];
        for j in 0..m {
            k1.push(h * f[j](t, w.clone()));
        }

        // Step 6 in Burden
        let mut k2 = vec![];
        let temp: Vec<f64> = k1
            .clone()
            .iter()
            .zip(&w)
            .map(|(k, w)| w + k / 2.0)
            .collect();
        for j in 0..m {
            k2.push(h * f[j](t + h / 2.0, temp.clone()));
        }

        // Step 7 in Burden
        let mut k3 = vec![];
        let temp: Vec<f64> = k2
            .clone()
            .iter()
            .zip(&w)
            .map(|(k, w)| w + k / 2.0)
            .collect();
        for j in 0..m {
            k3.push(h * f[j](t + h / 2.0, temp.clone()));
        }

        // Step 8 in Burden
        let mut k4 = vec![];
        let temp: Vec<f64> = k3.clone().iter().zip(&w).map(|(k, w)| w + k).collect();
        for j in 0..m {
            k4.push(h * f[j](t + h, temp.clone()));
        }

        // Step 9 in Burden
        for j in 0..m {
            w[j] += (k1[j] + 2.0 * k2[j] + 2.0 * k3[j] + k4[j]) / 6.0;
        }

        // Step 10 in Burden
        t += h;

        // Step 11 in Burden
        output.push((t, w.clone()));
    }
    output
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_single_equation() {
        // Burden, Example 3, pg. 289.
        let alpha = 0.5;
        let a = 0.0;
        let b = 2.0;
        let n = 10;

        let result = rk4_1d(|t, w| w - t * t + 1.0, n, alpha, a, b);
        println!("Problem 1");
        for res in &result {
            println!("{:?}", res);
        }

        assert!((result[10].1 - 5.3053630).abs() < 1e-6);
    }

    #[test]
    fn test_system_of_equations_1() {
        // Burden, Illustration, pg. 331.
        let mut f: Vec<fn(f64, Vec<f64>) -> f64> = vec![];
        f.push(|_t, w| -4.0 * w[0] + 3.0 * w[1] + 6.0);
        f.push(|_t, w| -2.4 * w[0] + 1.6 * w[1] + 3.6);
        let n = 5;
        let a = 0.0;
        let b = 0.5;
        let alpha = vec![0.0, 0.0];

        let result = rk4_nd(f, n, alpha, a, b);
        println!("Problem 2");
        for res in &result {
            println!("{:?}", res);
        }

        assert!((result[5].1[0] - 1.793505).abs() < 1e-5);
        assert!((result[5].1[1] - 1.014402).abs() < 1e-6);
    }

    #[test]
    fn test_system_of_equations_2() {
        // Burden, Example 1, pg. 334.
        let mut f: Vec<fn(f64, Vec<f64>) -> f64> = vec![];
        f.push(|_t, w| 1.0 * w[1]);
        f.push(|t, w| (2.0 * t).exp() * t.sin() - 2.0 * w[0] + 2.0 * w[1]);
        let n = 10;
        let a = 0.0;
        let b = 1.0;
        let alpha = vec![-0.4, -0.6];

        let result = rk4_nd(f, n, alpha, a, b);
        println!("Problem 3");
        for res in &result {
            println!("{:?}", res);
        }

        assert!((result[10].1[0] - -0.35339886).abs() < 1e-6);
        assert!((result[10].1[1] - 2.5787663).abs() < 1e-6);
    }
}
